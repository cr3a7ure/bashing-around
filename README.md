# BashingAround

Bash one liners and functions I use.

### Prompt/Env Vars

```bash
export PS1='\e\[\033[0;36m\]\w\e[0m $(terraform version | head -n1 | sed "s/Terraform //") \e\[\033[0;32m\]$(date -u "+%Y-%m-%d %H:%M:%S")\e[0m \e\[\033[0;33m\]$(git branch 2>/dev/null | grep '^*' | colrm 1 2)\e[0m \n $ '
```

add to `bash_profile` for mac `if [ -f "${HOME}/.bashrc" ]; then source "${HOME}/.bashrc"; fi`.


```bash
export CLICOLOR=1
export EDITOR="vim"
export LC_COLLATE="C"
```

---

### filefinder

There are many one liners for finding large files on linux systems this is one I put together a while back and use all the time.

```bash
function filefinder(){
  (find $(pwd) -type f -size +25M -print0 2> /dev/null | xargs -0 ls -lhsS) | column -t | cut -d ' ' -f 2- | sed -e 's/^[ \t]*//'
}
```

**Example:**
<pre>
[root@cj8dcl102 var]# (find $(pwd) -type f -size +25M -print0 2> /dev/null | xargs -0 ls -lhsS) | column -t | cut -d ' ' -f 2- | sed -e 's/^[ \t]*//'
-rw-------   1  root  root  401M  Sep  18  03:41  /var/log/messages-20170918
-rw-------   1  root  root  331M  Sep  24  03:27  /var/log/messages-20170924
-rw-------   1  root  root  293M  Sep  29  11:39  /var/log/messages
-rw-------   1  root  root  280M  Mar  7   2017   /var/lib/docker/tmp/GetImageBlob233346684
-rw-------   1  root  root  212M  Jun  15  2016   /var/lib/docker/tmp/GetImageBlob060374704
-rw-------   1  root  root  212M  Jun  15  2016   /var/lib/docker/tmp/GetImageBlob783178594
-rw-r--r--   1  root  root  203M  Sep  27  08:30  /var/cache/yum/x86_64/7OUL/base/gen/primary.xml.sqlite
-rw-r--r--   1  root  root  169M  Sep  27  07:10  /var/cache/yum/x86_64/7OUL/base/gen/primary.xml
-rw-r--r--   1  root  root  135M  Sep  27  08:30  /var/cache/yum/x86_64/7OUL/base_UEK4/gen/primary.xml.sqlite
-rw-r--r--   1  root  root  119M  Sep  27  07:15  /var/cache/yum/x86_64/7OUL/base_UEK4/gen/primary.xml
-rw-r--r--.  1  root  root  73M   Sep  1   13:26  /var/lib/rpm/Packages
-rw-------   1  root  root  60M   Jun  15  2016   /var/lib/docker/tmp/GetImageBlob061386980
-rw-------   1  root  root  41M   Apr  14  2016   /var/chef/cache/cookbooks/cars-automic/files/default/webhelpe.tar.gz
-rw-------   1  root  root  31M   Aug  3   10:30  /var/lib/docker/tmp/GetImageBlob025163980
</pre>

---

### fsuse

Used to fix display of df results and order filesystems from most used to least.

```bash
function fsuse(){
  df -hP | column -t | grep -v ^none | ( read header ; echo "$header" | sed 's/Mounted.*on/Mounted_on/g' ; sort -rn -k 5)
}
```


**Exmaple:**
<pre>
[kaddyman@cj8dcl102 ~]$ df -hP | column -t | grep -v ^none | ( read header ; echo "$header" | sed 's/Mounted.*on/Mounted_on/g' ; sort -rn -k 5)
Filesystem                      Size  Used  Avail  Use%  Mounted_on
/dev/mapper/rootvg-varlv        3.9G  2.4G  1.3G   65%   /var
/dev/mapper/appvg-splunklv      477M  282M  166M   63%   /apps/Splunk
/dev/mapper/rootvg-rootlv       2.0G  870M  964M   48%   /
/dev/sda1                       477M  186M  262M   42%   /boot
/dev/mapper/rootvg-usrlv        9.8G  3.9G  5.4G   42%   /usr
/dev/mapper/appvg-dockerlogslv  20G   4.0G  15G    22%   /apps/docker/logs
/dev/mapper/rootvg-optlv        3.9G  423M  3.2G   12%   /opt
tmpfs                           16G   1.7G  14G    11%   /run
/dev/mapper/rootvg-homelv       3.9G  84M   3.6G   3%    /home
/dev/mapper/rootvg-tmplv        3.9G  25M   3.6G   1%    /tmp
/dev/mapper/appvg-powertrainlv  4.8G  22M   4.6G   1%    /apps/powertrain
/dev/mapper/appvg-dockerlv      30G   45M   28G    1%    /apps/docker
tmpfs                           3.2G  0     3.2G   0%    /run/user/10056
tmpfs                           16G   0     16G    0%    /sys/fs/cgroup
tmpfs                           16G   0     16G    0%    /dev/shm
devtmpfs                        16G   0     16G    0%    /dev
</pre>

---

### showDeletedFiles

Used to find deleted files that are still associated with a running WebSphere Java process and list them from largest to smallest. If you are low on file system space but the file system is not showing any large files it might because of a java process holding on to it. Once you restart that java processes it will release that space back to the OS.

```bash
function showDeletedFiles {
  echo
  (
  echo "PID JVM FILESIZE FILENAME"
  (
  DELETEDFILES=$(/usr/sbin/lsof | grep \(del[e]ted\) | grep j[a]va| awk '{print $2","$7","$9}')
  JAVAPIDS=$(ps -ef |grep jav[a] | awk '{print $1","$2","$5","$NF}')

  for i in $(echo "${DELETEDFILES}"); do

    PID=$(echo "${i}" | awk -F',' '{print $1}')
    FILESIZE=$(echo "${i}" | awk -F',' '{print $2}')
    FILENAME=$(echo "${i}" | awk -F',' '{print $3}')

    if [[ ! "${FILESIZE}" == '0' ]]; then
      #echo "PID: ${PID} | FILESIZE: ${FILESIZE} | FILENAME: ${FILENAME}"

      for x in $(echo "${JAVAPIDS}"); do
        USER=$(echo "${x}" | awk -F',' '{print $1}')
        PSPID=$(echo "${x}" | awk -F',' '{print $2}')
        RUNAS=$(echo "${x}" | awk -F',' '{print $3}')
        JVMNAME=$(echo "${x}" | awk -F',' '{print $4}')

        #echo "User: ${USER} | PSPID: ${PSPID} | RUNAS: ${RUNAS} | JVMName: ${JVMNAME}"

        if [[ "${PID}" -eq "${PSPID}" ]]; then
          echo "${PSPID} ${JVMNAME} ${FILESIZE} ${FILENAME}"
        fi

      done

    fi

  done ) | sort -k 3 -n -r
  ) | column -t
  echo
}
```

**Example:**
<pre>
[cars@xxxxxxx ~]$ showDeletedFiles

PID    JVM                   FILESIZE   FILENAME
14563  Composite_Type1xxx01  789190585  /tmp/spring.log.7
27746  Composite_Type1xxx01  478772908  /tmp/spring.log.7
27746  Composite_Type2xxx01  478772908  /tmp/spring.log.7
19361  Composite_Type1xxx01  130243699  /tmp/spring.log.7
19361  Composite_Type2xxx01  36581879   /tmp/spring.log.1
27746  Composite_Type3xxx01  17926023   /tmp/spring.log.7
...
</pre>

---

### Useful bash functions for your bashrc

```bash
alias c="cd"
alias ..="cd .."
alias ..="cd ../.."
alias ...="cd ../../.."
alias ....="cd ../../../../"
alias sbrc="source ${HOME}/.bashrc"
alias j="cd ${HOME}/repos"

# Shows what my external IP is.
function myip(){
  dig +short myip.opendns.com @resolver1.opendns.com
}


function sglookup() {
  aws ec2 describe-network-interfaces --filters Name=group-id,Values=${1:-}
}

function blah() {
  junkfile=$(mktemp)
  vim ${junkfile}
  rm -rf ${junkfile}
}



```

### docker commands

map disk usage to running docker containers (update to overlay/overlay2 when needed)  
`for i in $(docker ps -q); do echo "$(docker ps | grep "${i}" | awk '{print $1,$2}')"; dockerFS=$(docker inspect ${i} | jq -r '.[].GraphDriver.Data | "\(.LowerDir)\n\(.MergedDir)"' | awk -F'/' '{print $6}'); for x in ${dockerFS[@]}; do du -sh /var/lib/docker/overlay2/${x}*; done; echo; done;`


map disk usage to all docker cotainers  
```
TOP_STORAGE=$(du -hs /var/lib/docker/overlay2/* | grep -Ee '^[0-9]{3}[M]+|[0-9]G' | sort -h |tail -n 10 |tee -a /dev/stderr |awk '{print $2}'|xargs|sed 's/ /|/g')
docker inspect $(docker ps -qa) | jq '.[]|.Config.Image,.GraphDriver.Data.MergedDir' | egrep -B2 "$TOP_STORAGE"
```

show the processes running in your container  
`docker top <dockerid> | sed -n '1!p' | sed 's/ \+/ /g'`


show the logs for your container  
`docker logs -f <dockerid> --tail 100`


show mem/cpu of your container  
`docker stats --no-stream=true $(docker ps -q) | sed -n '1!p' | sed 's/ \+/,/g' | awk -F',' '{print $1","$2","$3","$7}'`


force to be root  
`docker exec -u root -it <dockerid> /bin/bash`


log in with default user  
`docker exec -it <dockerid> /bin/bash`

#### git functions
```bash
function cgit() {
  # checkout master/main and pull
  if [[ "$PWD" =~ "repos/work/" ]]; then
    git checkout master
    git pull
  else
    git checkout main
    git pull
  fi
}

function fgit(){
  # fetch all remote branches
  git fetch --all
  git branch -a
}

function rgit() {
  # nuke option, will reset your repo like you just rm'd it and cloned it down again
  if [[ "$PWD" =~ "repos/work/" ]]; then
    cd $(git rev-parse --show-toplevel)
    git fetch origin
    git reset --hard origin/master
    git clean -fdx
  else
    cd $(git rev-parse --show-toplevel)
    git fetch origin
    git reset --hard origin/main
    git clean -fdx
  fi
}

function dgit() {
  # remove all local branches expect for master/main
  if [[ "$PWD" =~ "repos/work/" ]]; then
    git branch | grep -v "master" | xargs git branch -D
  else
    git branch | grep -v "main" | xargs git branch -D
  fi
}

function sgit() {
  # show files staged for push
  if [[ "$PWD" =~ "repos/work/" ]]; then
    git diff --stat --cached origin/master
  else
    git diff --stat --cached origin/main
  fi
}

function mgit() {
  # used when you want to update your branch with master/main
  if [[ "$PWD" =~ "repos/work/" ]]; then
    branch=$(git branch | grep \* | cut -d ' ' -f2)
    git checkout master
    git pull
    git checkout ${branch}
    git merge origin/master
  else
    branch=$(git branch | grep \* | cut -d ' ' -f2)
    git checkout main
    git pull
    git checkout ${branch}
    git merge origin/main
  fi
}

function jgit() {
  # jump to the root of your repo
  cd $(git rev-parse --show-toplevel)
}

function bgit() {
  # create a new branch, change to it and push it up
  # ex. bgit <YOUR_BRANCH_NAME>
  git branch "${1:-}"
  git checkout "${1:-}"
  git push -u origin "${1:-}"
}

function prgit() {
  # just run prgit in your branch to open a PR for it
  # made for github use but easy to modify
  if [[ -d "/Applications/Google Chrome.app" ]]; then
    INSTALLED="Google Chrome"
  elif [[ -d "/Applications/Firefox.app" ]]; then
    INSTALLED="firefox"
  elif [[ -d "/Applications/Safari.app" ]]; then
    INSTALLED="safari"
  else
    INSTALLED="none"
    printf "You don't have a supported browser (Chrome/Firefox/Safari) installed."
    exit 1
  fi

  repo=$(basename $(git rev-parse --show-toplevel))
  branch=$(git branch | grep \* | cut -d ' ' -f2)

  if [[ "$PWD" =~ "repos/work/" ]]; then
    open -a "${INSTALLED}" "https://github.com/sweetride/${repo}/pull/new/${branch}"
  else
    open -a "${INSTALLED}" "https://gitlab.com/kaddyman/${repo}/merge_requests/new?merge_request[source_branch]=${branch}&merge_request[target_branch]=main"
  fi
}

function ga() {
    # git add, if files is empty adds all
    if [ -z "$@" ]
    then
        git add .
    else
        git add "$@"
    fi
}

function gc() {
    # git commit, if message is empty, sets message to 'YOLO'
    if [ -z "$@" ]
    then
        git commit -m "YOLO"
    else
        git commit -m "$@"
    fi
}

function gp(){
    # git push
    git push
}

function gca() {
    # git adds all, commits with message 'YOLO', and pushes
    ga
    gc
    gp
}
```

### last grep you will ever need

```bash
# best grep there is. ignore .git and .terraform folders
function gf() {
  grep -iR --exclude-dir={.terraform,.git} ''"${1:-}"'' '.'
}
```

### aws auth and misc

```bash
# If the CLI has MFA you can use this to generate keys by passing it the MFA code
function mfa-gen() 
aws --profile ken sts get-session-token --serial-number arn:aws:iam::70049999517:mfa/ken.addyman --token-code ${1:-} | jq -r '.Credentials | "aws_access_key_id=\(.AccessKeyId)\naws_secret_access_key=\(.SecretAccessKey)\naws_session_token=\(.SessionToken)"'
}
```

```bash
function ecr-login() {
  if [[ "$PWD" =~ "repos/work/" ]]; then
    aws_region="us-east-2"
  else
    aws_region="us-east-1"
  fi
  # AWS CLI V1 ecr login
  #$(aws ecr get-login --no-include-email)

  # If on mac be sure to update your shell in user account settings
  # otherwise bash v3 is used and v3 does not support declarative arrays
  # AWS CLI V2 ecr login
  searchstring=${1:-"null"}
  declare -A aws_accounts
  aws_accounts[kac]="wasd"
  aws_accounts[reaper]="lol"
  if test "${aws_accounts[${searchstring}]+isset}"; then
    AWS_PROFILE=$searchstring aws ecr get-login-password | docker login --password-stdin --username AWS "${aws_accounts[$searchstring]}.dkr.ecr.${aws_region}.amazonaws.com"
  else
    for x in "${!aws_accounts[@]}"; do printf "[%s]=%s\n" "$x" "${aws_accounts[$x]}" ; done
  fi
}
```

```bash
function aws-unset() {
    #This will unset all aws env vars
    unset AWS_ACCESS_KEY_ID
    unset AWS_SECRET_ACCESS_KEY
    unset AWS_SESSION_TOKEN
    unset AWS_REGION
    unset AWS_DEFAULT_REGION

    env | grep -q AWS
    if [[ $? -eq 0  ]]; then
      for i in $(env | grep AWS | awk -F'=' '{print $1}'); do
        if [[ ! "${i}" =~ "AWS_PROFILE" ]]; then
          echo "unsetting ${i}"
          unset ${i}
        fi
      done
    fi
    echo "all aws env variables except for AWS_PROFILE have been unset"
}
```

```bash
function aws-auth(){
    aws-unset

    if [[ -z "${AWS_PROFILE:-}" ]]; then
      echo 'Please set $AWS_PROFILE.' >&2
      return 1
    fi

    if aws configure get source_profile &>/dev/null; then
      region="us-east-2"
      local creds source_profile arn

      source_profile=$(aws configure get source_profile)
      arn=$(aws configure get role_arn)
      role_name=$(echo "$arn" | awk -F'/' '{print $NF}')
      creds=$(AWS_PROFILE="$source_profile" aws sts assume-role --role-arn "$arn" --role-session-name "${USER}-${role_name}" --duration-seconds 3600)

      printf "\nYou local env has been setup with env variables for local use in ${AWS_PROFILE} if you want them. otherwise run aws-unset to clear them.\n\n"
      export AWS_ACCESS_KEY_ID="$(echo "$creds" | jq -r .Credentials.AccessKeyId)"
      export AWS_SECRET_ACCESS_KEY="$(echo "$creds" | jq -r .Credentials.SecretAccessKey)"
      export AWS_SESSION_TOKEN="$(echo "$creds" | jq -r .Credentials.SessionToken)"
      export AWS_REGION=${region}
      export AWS_DEFAULT_REGION=${region}

      #printf "You can set these inside your app for temp access in ${AWS_PROFILE}. keys are good for 1 hour\n\n"
      #echo "AWS_ACCESS_KEY_ID=$(echo $creds | jq -r .Credentials.AccessKeyId)"
      #echo "AWS_SECRET_ACCESS_KEY=$(echo $creds | jq -r .Credentials.SecretAccessKey)"
      #echo "AWS_SESSION_TOKEN=$(echo $creds | jq -r .Credentials.SessionToken)"
      #echo "AWS_REGION=${region}"
      #echo "AWS_DEFAULT_REGION=${region}"
    fi
}
```

### Random

```bash
# create a temp file and delete on close
function blah() {
  junkfile=$(mktemp)
  vim ${junkfile}
  rm -rf ${junkfile}
}
```

```bash
# get ssl cert info
function sslg() {
  cfssl certinfo -domain "${1:-}"
}
```

```bash
# delete by inode number
function irm() {
  find . -inum "${1:-}" -exec rm -i {} \;
}
```

```bash
# zip the file 
# zipit filedir/ filezip.zip
function zipit() {
  zip -r "${2}" "${1}" -x "*.DS_Store" -x "__MACOSX" -x "*._Filename"
}

# no sleep
function nosleep() {
  caffeinate -dis
}

function pubkey() {
  # generate pubkey from private
  ssh-keygen -y -f ${1:-}
}

function rsakey() {
  ssh-keygen -t rsa -b 4096 -m PEM -f "${1:-lol}.pem"
}

function ppk-gen() {
  puttygen ${1:-} -o ${1:-}.ppk -O private
}

function vl() {
  # vault login example
  vault_role=${1:-devops}
  AWS_PROFILE=kdc aws-auth
  vault login -method=aws role="${vault_role}"
  aws-unset
}
```

```bash
# just some dns functions
function dns() {
  printf "\n=================================\ngoogle dns 8.8.8.8 results\n\n"
  dig "${1:-google.com}" +short @8.8.8.8
  printf "\n=================================\ncloudflare dns 1.1.1.1 results\n\n"
  dig "${1:-google.com}" +short @1.1.1.1
  printf "\n=================================\nYour local dns results check resolv.conf\n\n"
  dig "${1:-google.com}" +short
  echo
}

function dnsall() {
  printf "\n=================================\ngoogle dns 8.8.8.8 results\n\n"
  dig +nocmd "${1:-google.com}" ANY +multiline +noall +answer @8.8.8.8
  printf "\n=================================\nYour local dns results check resolv.conf\n\n"
  dig +nocmd "${1:-google.com}" ANY +multiline +noall +answer
  echo
}
```

---

### Terraform

```bash
# used for terraform 0.11 with tfvar files
function tf() {
    unset AWS_ACCESS_KEY_ID
    unset AWS_SECRET_ACCESS_KEY
    unset AWS_SESSION_TOKEN
    unset AWS_REGION
    unset AWS_DEFAULT_REGION

    tfenv use 0.11.11
    if AWS_PROFILE=${1:-} aws configure get source_profile &>/dev/null; then
      local creds source_profile arn

      source_profile="$(AWS_PROFILE=${1:-} aws configure get source_profile)"
      arn="$(AWS_PROFILE=${1:-} aws configure get role_arn)"
      creds="$(AWS_PROFILE="$source_profile" aws sts assume-role --role-arn "$arn" --role-session-name "${USER}--terraform" --duration-seconds 3600)"

      export AWS_ACCESS_KEY_ID="$(echo "$creds" | jq -r .Credentials.AccessKeyId)"
      export AWS_SECRET_ACCESS_KEY="$(echo "$creds" | jq -r .Credentials.SecretAccessKey)"
      export AWS_SESSION_TOKEN="$(echo "$creds" | jq -r .Credentials.SessionToken)"
    fi

    if [[ "${2:-}" =~ "init" && -d "./.terraform" ]]; then
        echo "removing .terraform folder so caching stops making me angry"
        rm -rf .terraform/
    fi

    terraform fmt
    terraform "${2:-}" -var-file=~/.terrified/${1:-}.tfvars ${3:-} ${4:-}
}
```

```bash
function ta() {
  aws-unset
  tfenv use 0.13.4

  if [[ "${1:-}" =~ "init" && -d "./.terraform" ]]; then
      echo "removing .terraform folder so caching stops making me angry"
      rm -rf .terraform/
  fi

  terraform fmt
  if [[ "$PWD" =~ "repos/work/" ]]; then
    PROFILE_NAME="control"
  else
    PROFILE_NAME="ken"
  fi
  AWS_PROFILE="$PROFILE_NAME" terraform $@
}
```

----

### Bash code challenges

**file contents:**

The quick brown fox jumps over the lazy dog.

**Letter Count: (use sed "$ d" to remove last line if needed)**
```bash
(for i in $(grep -o . file); do echo ${i}; done;) | tr '[[:upper:]]' '[[:lower:]]' | sed 's/[^a-z]*//g' | sort -f | uniq -ic | sort -rk 1,1
```

**Word Count:**
```bash
(for i in $(cat file); do echo ${i}; done;) | tr '[[:upper:]]' '[[:lower:]]' | sed 's/[^a-z]*//g' | sort -f | uniq -ic | sort -rk 1,1
```

```bash
#!/usr/bin/env bash

# Suppose you are given an array of size 1 to n-length. Write a function that
# will log to console which values between 1 and n that are NOT in the array.
# For example - given an array of size 5 containing values [5, 2, 5, 1, 1] your
# function would print to console "3,4". On the other hand, given an array of
# size 6 that contained values [6, 3, 4, 1, 2, 5], your function would print nothing ("").

set -oue pipefail

function ohgod_what_i_have_done() {
  array=$1
  declare -a dedupsorted
  declare -a newlist

  IFS=$'\n'
  sorted=($(echo "${array[@]}" | tr ' ' '\n' | sort -nu))
  unset IFS

  last=$(echo "${sorted[-1]}")
  first=$(echo "${sorted[0]}")

  IFS=$'\n'
  for i in $(seq ${first} ${last}); do
    newlist+=("${i}")
  done
  unset IFS

  echo ${newlist[@]} ${sorted[@]} | tr ' ' '\n' | sort -n | uniq -u
}

function main() {
  if [[ -z "${1:-}" ]]; then echo "example: $0 \"1,12,3,6,17\""; exit 1; fi
  IFS=', ' read -r -a array <<< "$1"
  ohgod_what_i_have_done "${array[@]}"
}

main "$@"
```



```bash
#!/usr/bin/env bash

# Write a function that converts the array of values below to an object of
# key/value pairs with each value’s index as the key in reverse order.

set -oue pipefail

function ohgod_what_i_have_done() {
  array=$1
  declare -A HASHTABLE
  count=0

  len="${#array[@]}"
  len=$((len-1))

  # leaving this in because it's a neat trick.
  #reversed=($(printf '%s\n' "${array[@]}" | tac | tr '\n' ' '))

  for i in $(seq ${len} -1 0 ); do
    HASHTABLE+=(["$i"]="${array[$count]}")
    let count+=1
  done

  for x in "${!HASHTABLE[@]}"; do
    printf "[%s]=%s\n" "$x" "${HASHTABLE[$x]}"
  done
}

function main() {
  if [[ -z "${1:-}" ]]; then echo "example: $0 \"5,8,136,16,92\""; exit 1; fi
  IFS=', ' read -r -a array <<< "$1"
  ohgod_what_i_have_done "${array[@]}"
}

main "$@"
```



```bash
#!/usr/bin/env bash
set -oui pipefail
#set -x

# write two functions, one converts decimals to roman numerals the other
# converts roman numerals to decimals. You will need to detect what they are
# passing and route to the correct function. You will also check if what is being
# passsed in is a roman numeral/decimal and exit if not. It must also support
# passing in lower case roman numerals.
#
# example
# $ ./test mcmlxxxix
# mcmlxxxix is 1989
#
# $ ./test MCMLXXXIX
# MCMLXXXIX is 1989
#
# $ ./test 1989
# 1989 is MCMLXXXIX
#
# $ ./test 0---9
# not a number or roman numeral, exiting...

function num_to_roman() {
  dec_value=$1
  value=("1000" "900" "500" "400" "100" "90" "50" "40" "10" "9" "5" "4" "1")
  roman=("M" "CM" "D" "CD" "C" "XC" "L" "XL" "X" "IX" "V" "IV" "I")
  counter=0
  romanized=

  for i in "${value[@]}"; do
    while [[ $i -le $dec_value ]]; do
      romanized+="${roman[$counter]}"
      let dec_value-="${value[$counter]}"
    done
    let counter+=1
  done

  printf "$romanized\n"
}

function roman_to_num() {
  declare -A romans
  romans["M"]="1000"
  romans["D"]="500"
  romans["C"]="100"
  romans["L"]="50"
  romans["X"]="10"
  romans["V"]="5"
  romans["I"]="1"

  per=0
  dec_value=0
  roman_num=$1

  # reverse the string before making it an array
  rev=
  copy=$roman_num
  len=${#copy}
  for((i=$len-1;i>=0;i--)); do rev="$rev${copy:$i:1}"; done

  # if you are using the reverse string method use $rev if using the
  # reverse array method below use $roman_num in the for loop
  roman_num_arr=()
  for i in $(echo "$rev" | grep -o .); do
    roman_num_arr+=("$i")
  done

  # reverse the array
  #roman_num_arr=($(printf '%s\n' "${roman_num_arr[@]}" | tac | tr '\n' ' '))

  for i in $(seq 0 $((${#roman_num_arr[@]}-1))); do
    if [[ ${romans[${roman_num_arr[$i]}]} -ge $per ]]; then
      let dec_value+=${romans[${roman_num_arr[$i]}]}
    else
      let dec_value-=${romans[${roman_num_arr[$i]}]}
    fi
    per=${romans[${roman_num_arr[$i]}]}
  done

  printf "$dec_value\n"
}

function main() {
  if [[ -z "${1:-}" ]]; then printf "example: $0 1982\nexample: $0 MCMLXXXIX\n"; exit 1; fi
  if [[ $1 =~ ^[0-9]+$ ]]; then
    answer=$(num_to_roman ${1:-})
  elif [[ $1 =~ ^[a-zA-Z]+$ ]]; then
    upper=$(echo "$1" | tr '[[:lower:]]' '[[:upper:]]')
    answer=$(roman_to_num ${upper})
  else
    echo "not a number or roman numeral, exiting..."
    exit 1
  fi
  printf "$1 is $answer\n"
}

main "$@"
```
